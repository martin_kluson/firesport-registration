CREATE TABLE Muzi (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Zeny (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Veterani (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Muzi_duel (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Muzi_duel_zaloznici (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Zeny_duel (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Zeny_duel_zaloznici (
    id INTEGER PRIMARY KEY,
    state TEXT,
    value TEXT,
    owner INTEGER
);

CREATE TABLE Locked (
    id INTEGER PRIMARY KEY,
    locked INTEGER
);

CREATE TABLE User (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER UNIQUE NOT NULL
);

INSERT INTO Locked (locked) VALUES (1);
