from flask import Flask, render_template, request, redirect, make_response, session
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData
from flask_socketio import SocketIO, emit
from flask_httpauth import HTTPBasicAuth

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///messages.db'
app.config['SECRET_KEY'] = 'your_secret_key'
db = SQLAlchemy(app)
socketio = SocketIO(app)
auth = HTTPBasicAuth()

users = {
    "admin": "passwd"
}

created_table_classes = {}

@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None

def load_data_tables():
    metadata = MetaData()
    metadata.reflect(bind=db.engine)

    for table_name in metadata.tables.keys():
        if table_name not in ('Locked', 'User'):
            get_table_class(table_name)

def get_table_class(table_name):

    if table_name in created_table_classes:
        return created_table_classes[table_name]

    class DataModel(db.Model):
        __tablename__ = table_name
        id = db.Column(db.Integer, primary_key=True)
        state = db.Column(db.String(10))
        value = db.Column(db.String(100))
        owner = db.Column(db.Integer)

    created_table_classes[table_name] = DataModel
    return DataModel

class Locked(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    locked = db.Column(db.Integer)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, unique=True, nullable=False)

def get_next_user_id():
    last_user = User.query.order_by(User.user_id.desc()).first()
    if last_user:
        return last_user.user_id + 1
    else:
        return 100

def index_page(admin):
    user_id = request.cookies.get('user_id')
    user_id = int(user_id) if user_id else 0

    locked = Locked.query.first()

    tables_content = {}

    for table in created_table_classes:
        tables_content[table] = get_table_class(table).query.all()
    response = make_response(render_template('index.html', tables_content=tables_content, user_id=user_id, admin=admin, locked=locked.locked))
    if not user_id:
        next_user_id = get_next_user_id()
        new_user = User(user_id=next_user_id)
        db.session.add(new_user)
        db.session.commit()

        response.set_cookie('user_id', str(next_user_id))

    return response

@app.route('/')
def index():
    session['admin'] = False
    return index_page(admin=session['admin'])

@app.route('/take', methods=['POST'])
def take():
    row_id = request.form['row_id']
    table_name = request.form['table_name']
    user_id = int(request.cookies.get('user_id'))

    edit_in_progress = get_table_class(table_name).query.filter_by(state="edit", owner=user_id).first()
    if edit_in_progress:
        return redirect('/')

    row = get_table_class(table_name).query.filter_by(id=row_id).first()

    if row.state == "free" or (row.state == 'taken' and user_id == row.owner) or session['admin']:
        row.owner = user_id
        row.state = "edit"
        db.session.commit()
        socketio.emit('update')

    if session['admin']:
        return redirect('/admin')
    else:
        return redirect('/')

@app.route('/update_value', methods=['POST'])
def update_value():
    row_id = request.form['row_id']
    table_name = request.form['table_name']
    new_value = request.form[f"value_input_{table_name}_{row_id}"]

    row = get_table_class(table_name).query.filter_by(id=row_id).first()

    row.value = new_value

    if new_value:
        row.state = "taken"
    else:
        row.state = "free"
        row.owner = 0

    db.session.commit()
    socketio.emit('update')

    if session['admin']:
        return redirect('/admin')
    else:
        return redirect('/')

@app.route('/admin')
@auth.login_required
def admin():
    session['admin'] = True
    return index_page(admin=session['admin'])

@app.route('/add_line', methods=['POST'])
def add_line():
    table_name = request.form['table_name']

    new_data_row = get_table_class(table_name)(state='free', value='', owner=0)

    db.session.add(new_data_row)

    db.session.commit()
    socketio.emit('update')
    return redirect('/admin')

@app.route('/delete_line', methods=['POST'])
def delete_line():
    table_name = request.form['table_name']

    last_row = get_table_class(table_name).query.order_by(get_table_class(table_name).id.desc()).first()

    if last_row:
        db.session.delete(last_row)
        db.session.commit()
        socketio.emit('update')

    return redirect('/admin')

@app.route('/toggle_locked', methods=['POST'])
def toggle_locked():
    locked_row = Locked.query.first()
    if locked_row:
        locked_row.locked = not locked_row.locked
        db.session.commit()
        socketio.emit('update')

    return redirect('/admin')

with app.app_context():
    load_data_tables()

if __name__ == '__main__':
    app.run()
