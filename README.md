# Firesport registration

TBD

## Dependencies

``pip install Flask Flask-SQLAlchemy Flask-SocketIO gunicorn eventlet``

## Author
Martin Klusoň (martin@kluson.cz)

## License
GPL v3