var socket = io();

socket.on('update', function(data) {
    const isEditing = document.querySelector('input[name^="value_input_"]');

    if (!isEditing) {
        location.reload();
    }
});

window.addEventListener('beforeunload', () => {
    // Uložení pozice stránky
    localStorage.setItem('scrollPosition', window.scrollY);

    // Uložení hodnot textových polí
    const textInputs = document.querySelectorAll('input[name^="value_input_"]');
    textInputs.forEach((textInput) => {
        localStorage.setItem(textInput.name, textInput.value);
    });
});

window.addEventListener('load', () => {
    // Obnovení pozice stránky
    const scrollPosition = localStorage.getItem('scrollPosition');
    if (scrollPosition !== null) {
        window.scrollTo(0, parseInt(scrollPosition, 10));
    }

    // Obnovení hodnot textových polí
    const textInputs = document.querySelectorAll('input[name^="value_input_"]');
    textInputs.forEach((textInput) => {
        const savedValue = localStorage.getItem(textInput.name);
        if (savedValue) {
            textInput.value = savedValue;
        }
    });

    // Obnovení hodnoty cookie 'user_id' z localStorage, pokud je potřeba
    const localStorageUserId = localStorage.getItem('user_id');
    const currentCookieUserId = document.cookie.split('; ').find(row => row.startsWith('user_id='));

    if (localStorageUserId) {
        const currentCookieValue = currentCookieUserId ? currentCookieUserId.split('=')[1] : null;

        if (localStorageUserId !== currentCookieValue) {
            // Pokud se hodnota v localStorage liší od cookie, aktualizujeme cookie a provedeme reload
            document.cookie = `user_id=${localStorageUserId}; path=/`;
            window.location.reload();
        }
    }
});

document.querySelectorAll('.editForm').forEach(form => {
    form.addEventListener('submit', function(event) {
        // Uložení hodnoty cookie 'user_id' do localStorage
        const userId = document.cookie.split('; ').find(row => row.startsWith('user_id='));
        if (userId) {
            localStorage.setItem('user_id', userId.split('=')[1]);
        }

        const isEditing = document.querySelector('input[name^="value_input_"]');

        if (isEditing) {
            event.preventDefault();
            isEditing.style.border = '2px solid #e57373';
            isEditing.style.boxShadow = '0 0 5px rgba(255, 0, 0, 0.5)';

             isEditing.scrollIntoView({
                behavior: 'smooth',
                block: 'center'
            });
        }
    });
});