---
- name: Setup and configure servers
  hosts: all

  tasks:
    - name: Disable SELinux temporarily
      command: setenforce 0
      become: yes
      ignore_errors: yes
      changed_when: false

    - name: Persistently disable SELinux
      lineinfile:
        path: /etc/selinux/config
        regexp: '^SELINUX='
        line: 'SELINUX=disabled'
      become: yes

    - name: Install required packages
      yum:
        name: "{{ item }}"
        state: present
      become: yes
      loop:
        - python3-pip
        - git
        - nginx
        - vim
        - sqlite

    - name: Remove firesport-registration directory
      ansible.builtin.file:
        path: /home/ec2-user/firesport-registration
        state: absent

    - name: Clone git repository
      git:
        repo: https://gitlab.com/martin_kluson/firesport-registration.git
        dest: /home/ec2-user/firesport-registration

    - name: Create Python virtual environment
      ansible.builtin.command:
        cmd: /usr/bin/python3 -m venv /home/ec2-user/firesport-registration/venv
        creates: /home/ec2-user/firesport-registration/venv/bin/python

    - name: Install Python packages using pip
      pip:
        virtualenv: /home/ec2-user/firesport-registration/venv
        name:
          - Flask
          - Flask-SQLAlchemy
          - Flask-SocketIO
          - flask-httpauth
          - gunicorn
          - eventlet

    - name: Ensure instance directory exists
      file:
        path: /home/ec2-user/firesport-registration/instance
        state: directory
        owner: ec2-user
        group: ec2-user

    - name: Initialize SQLite database
      ansible.builtin.shell: sqlite3 /home/ec2-user/firesport-registration/instance/messages.db < /home/ec2-user/firesport-registration/init_db.sql
      args:
        creates: /home/ec2-user/firesport-registration/instance/messages.db

    - name: Create myapp systemd service file
      ansible.builtin.template:
        src: myapp.service.j2
        dest: /etc/systemd/system/myapp.service
      become: yes
      vars:
        user: ec2-user
        group: nginx
        app_path: /home/ec2-user/firesport-registration
        venv_path: /home/ec2-user/firesport-registration/venv

#    - name: Add location block to nginx.conf
#      ansible.builtin.lineinfile:
#        path: /etc/nginx/nginx.conf
#        insertafter: '^ *include /etc/nginx/default.d/.*\.conf;'
#        line: |
#          location / {
#              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
#              proxy_set_header Host $host;
#              proxy_pass http://localhost:8000;
#              proxy_http_version 1.1;
#              proxy_set_header Upgrade $http_upgrade;
#              proxy_set_header Connection "upgrade";
#          }
#      become: yes

    - name: Ensure systemd services are enabled
      systemd:
        name: "{{ item }}"
        enabled: yes
      loop:
        - nginx
        - myapp
      become: yes

    - name: Restart systemd services
      systemd:
        name: "{{ item }}"
        state: restarted
      loop:
        - nginx
        - myapp
      become: yes


